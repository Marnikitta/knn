package org.tomat.spbsu.ml;

import java.io.Serializable;

public class Entry implements Serializable{
    private int dim;
    private int[] point;
    private int classId;

    public Entry(int[] point, int classId) {
        this.point = point;
        this.classId = classId;
        dim = point.length;
    }

    public int[] getPoint() {
        return point;
    }

    public int getClassId() {
        return classId;
    }

    public int getDim() {
        return dim;
    }
}

