package org.tomat.spbsu.ml.knn;

import org.tomat.spbsu.ml.Entry;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BootstrapKNN {
    private final static int features = 100;
    private int dim = 28 * 28;
    protected List<Entry> entries;
    ArrayList<LSHKNNClassifier> forest = new ArrayList<>();
    private int classes = 10;

    public BootstrapKNN(List<Entry> entries) {
        Random rd = new Random();
        this.entries = entries;
        boolean[] features = new boolean[dim];
        for (int i = 0; i < dim; ++i) {
            if (rd.nextInt() % 10 == 0) {
                features[i] = true;
            }
        }
        for (int i = 0; i < Math.sqrt((double) entries.size()); ++i) {
            forest.add(new LSHKNNClassifier(generate(entries), rd.nextInt(7) + 7, 140 + rd.nextInt(20), 1500, 50));
            System.out.println(i + " learned");
        }
    }

    public List<Entry> generate(List<Entry> data) {
        ArrayList<Entry> result = new ArrayList<>();
        Random rd = new Random();
        for (int i = 0; i < data.size(); ++i) {
            result.add(data.get(rd.nextInt(data.size())));
        }
        return result;
    }

    public int getClassId(int[] point) {
        Random rd = new Random();
        int[] results = new int[classes];
        for (int i = 0; i < results.length; ++i) {
            results[forest.get(i).getClassId(point)]++;
        }
        int maxClass = 0;
        for (int i = 0; i < results.length; ++i) {
            if (results[i] > results[maxClass]) {
                maxClass = i;
            }
        }
        return maxClass;
    }

}
