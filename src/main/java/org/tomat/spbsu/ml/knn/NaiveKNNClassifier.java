package org.tomat.spbsu.ml.knn;


import org.tomat.spbsu.ml.Entry;

import java.util.*;

public class NaiveKNNClassifier {

    private List<Entry> entries = new ArrayList<>();

    private final int dimensions = 28 * 28;
    private int nei = 10;
    private int classes = 10;

    public NaiveKNNClassifier(List<Entry> entries, int neighbours) {
        this.entries = entries;
        this.nei = neighbours;
    }

    public void setNei(int nei) {
        this.nei = nei;
    }

    public Queue<Pair> distances(int[] point) {
        if (point.length != dimensions) {
            throw new IllegalArgumentException();
        }
        Queue<Pair> queue = new PriorityQueue<>(entries.size());
        for (Entry dp : entries) {
            queue.add(new Pair(dp, squareDistanceTo(point, dp.getPoint())));
        }
        return queue;
    }

    public int getClassId(int[] point) {
        Queue<Pair> dist = distances(point);
        int[] c = new int[classes];
        for (int i = 0; i < nei && !dist.isEmpty(); ++i) {
            Entry dp = dist.poll().getFirst();
            c[dp.getClassId()]++;
        }
        int max = -1;
        int classMax = -1;
        for (int i = 0; i < classes; ++i) {
            if (c[i] > max) {
                max = c[i];
                classMax = i;
            }
        }
        return classMax;
    }

    public static double squareDistanceTo(int[] p1, int[] p2) {
        double result = 0;
        for (int i = 0; i < Math.min(p1.length, p2.length); ++i) {
            result += (p1[i] - p2[i]) * (p1[i] - p2[i]);
        }
        return result;
    }

    public static class Pair implements Comparable<Pair> {
        Entry first;
        double distance;

        public Pair(Entry first, double distance) {
            this.first = first;
            this.distance = distance;
        }

        @Override
        public int compareTo(Pair o) {
            return Double.compare(distance, o.distance);
        }


        public double getDistance() {
            return distance;
        }

        public Entry getFirst() {

            return first;
        }
    }
}

