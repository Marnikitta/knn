package org.tomat.spbsu.ml.knn;

import org.tomat.spbsu.ml.Entry;
import org.tomat.spbsu.ml.hashing.util.Neighbours;

import java.io.Serializable;
import java.util.*;

public class LSHKNNClassifier implements Serializable {

    private Neighbours neighbours;
    private int nei;
    private int l;
    private int k;
    private int radius;


    public void setNei(int nei) {
        this.nei = nei;
    }

    private int classes = 10;

    public LSHKNNClassifier(List<Entry> entries, int k, int l, int r, int nei) {
        neighbours = new Neighbours(entries, k, l, r);
        this.nei = nei;
        this.l = l;
        this.k = k;
        this.radius = r;
    }


    public Queue<Pair> distances(int[] point) {
        HashSet<Entry> candidates = neighbours.getNN(point);
        Queue<Pair> queue = new PriorityQueue<>(candidates.size());
        for (Entry dp : candidates) {
            queue.add(new Pair(dp, squareDistanceTo(point, dp.getPoint())));
        }
        return queue;
    }

    public int getClassId(int[] point) {
        Queue<Pair> dist = distances(point);
        int[] c = new int[classes];
        for (int i = 0; i < nei && !dist.isEmpty(); ++i) {
            Entry dp = dist.poll().getFirst();
            c[dp.getClassId()]++;
        }
        int max = -1;
        int classMax = -1;
        for (int i = 0; i < classes; ++i) {
            if (c[i] > max) {
                max = c[i];
                classMax = i;
            }
        }
        return classMax;
    }

    public double squareDistanceTo(int[] p1, int[] p2) {
        double result = 0;
        for (int i = 0; i < Math.min(p1.length, p2.length); ++i) {
            result += (p1[i] - p2[i]) * (p1[i] - p2[i]);
        }
        return result;
    }

    public static class Pair implements Comparable<Pair> {
        Entry first;
        double distance;

        public Pair(Entry first, double distance) {
            this.first = first;
            this.distance = distance;
        }

        @Override
        public int compareTo(Pair o) {
            return Double.compare(distance, o.distance);
        }


        public Entry getFirst() {
            return first;
        }
    }

    public int getNei() {
        return nei;
    }

    public int getL() {
        return l;
    }

    public int getK() {
        return k;
    }

    public int getRadius() {
        return radius;
    }
}

