package org.tomat.spbsu.ml;

import org.tomat.spbsu.ml.tree.Tree;

import java.io.*;
import java.util.ArrayList;

public final class Loader {
    private Loader() {
    }

    public static ArrayList<Entry> loadEntries(InputStream pictFP, InputStream labelsFP, int size) throws IOException {
        ArrayList<int[]> data = getDataFromFile(pictFP, size);
        ArrayList<Integer> labels = getLabelsFromFile(labelsFP, size);
        ArrayList<Entry> result = new ArrayList<>();
        for (int i = 0; i < Math.min(data.size(), labels.size()); ++i) {
            result.add(new Entry(data.get(i), labels.get(i)));
        }
        return result;
    }

    public static ArrayList<int[]> getDataFromFile(InputStream fp, int size) throws IOException {

        ArrayList<int[]> result = new ArrayList<>();
        DataInputStream dt = new DataInputStream(new BufferedInputStream(fp));
        int magic = dt.readInt();
        int number = dt.readInt();
        int height = dt.readInt();
        int width = dt.readInt();

        System.out.println(magic + " " + number + " " + height + " " + width);

        int n = Math.min(size, number);

        for (int i = 0; i < n; ++i) {
            int[] buffer = new int[height * width];
            for (int j = 0; j < height * width; ++j) {
                buffer[j] = dt.readUnsignedByte();
            }
            result.add(buffer);
        }

        return result;
    }


    public static ArrayList<Integer> getLabelsFromFile(InputStream fp, int size) throws IOException {
        ArrayList<Integer> result = new ArrayList<>();
        DataInputStream dt = new DataInputStream(new BufferedInputStream(fp));
        int magic = dt.readInt();
        int number = dt.readInt();

        System.out.println(magic + " " + number);

        int n = Math.min(size, number);

        for (int i = 0; i < n; ++i) {
            int r = dt.readUnsignedByte();
            result.add(r);
        }
        return result;
    }
}

