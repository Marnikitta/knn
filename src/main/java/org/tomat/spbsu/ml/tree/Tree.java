package org.tomat.spbsu.ml.tree;

import org.tomat.spbsu.ml.Entry;

import java.util.*;

public class Tree {
    private int classes = 10;
    private int dim = 28 * 28;
    private int lower = 0;
    private int upper = 255;
    private Node root;

    private class Node {
        private Node leftChild;
        private Node rightChild;
        private boolean isLeave = true;
        private DivisionPoint divisionPoint;
        private int classId;
        private double entropy = Double.POSITIVE_INFINITY;

        private List<Entry> entryList;

        public Node(List<Entry> entryList) {
            this.entryList = entryList;

            calcEntropy();
            System.out.println(entryList.size());

            if (this.entropy <= 0.15) {
                makeAsLeave();
            }
        }

        public void calcEntropy() {
            int[] c = new int[classes];

            for (Entry e : entryList) {
                c[e.getClassId()]++;
            }

            this.entropy = entropyForP(c);
        }

        public void makeAsLeave() {
            this.isLeave = true;

            int[] c = new int[classes];
            for (Entry e : entryList) {
                c[e.getClassId()]++;
            }

            int minI = 0;
            for (int i = 0; i < c.length; ++i) {
                if (c[i] > c[minI]) {
                    minI = i;
                }
            }
            this.classId = minI;
        }


        public int getClassId(int[] point) {
            if (isLeave) {
                return classId;
            } else {
                if (!divisionPoint.side(point)) {
                    return leftChild.getClassId(point);
                } else {
                    return rightChild.getClassId(point);
                }
            }
        }

        public boolean split(boolean[] features) {
            if (entropy == 0) {
                return false;
            }


            DivisionPoint point = divide(entryList, features);

            ArrayList<Entry> left = new ArrayList<>();
            ArrayList<Entry> right = new ArrayList<>();

            for (Entry e : entryList) {
                if (!point.side(e.getPoint())) {
                    left.add(e);
                } else {
                    right.add(e);
                }
            }

            this.divisionPoint = point;
            this.leftChild = new Node(left);
            this.rightChild = new Node(right);
            this.isLeave = false;

            this.entryList.clear();

            return true;
        }
    }

    public static double entropyForP(int[] c) {
        int n = 0;
        for (int i : c) {
            n += i;
        }
        double entropy = 0;
        for (int aC : c) {
            if (aC != 0) {
                double p = (double) aC / n;
                entropy += p * Math.log(p) / Math.log(2);
            }
        }
        return -entropy;
    }

    public double averageEntropy(List<Entry> data, int feature, int limit) {
        int[] leftClassCount = new int[classes];
        int[] rightClassCount = new int[classes];
        boolean lC = false;
        boolean rC = false;

        for (Entry e : data) {
            if (e.getPoint()[feature] < limit) {
                lC = true;
                leftClassCount[e.getClassId()]++;
            } else {
                rC = true;
                rightClassCount[e.getClassId()]++;
            }
        }

        if (!lC || !rC) {
            return Double.POSITIVE_INFINITY;
        }

        double leftEntropy = entropyForP(leftClassCount);
        double rightEntropy = entropyForP(rightClassCount);

        return (leftEntropy + rightEntropy) / 2;
    }

    public DivisionPoint divide(List<Entry> data, boolean[] enabled) {
        int minFeature = -1;
        int minLimit = lower;

        double minEntropy = Double.POSITIVE_INFINITY;

        for (int i = 0; i < dim; ++i) {
            if (enabled[i]) {
                for (int j = lower; j <= upper; j = j + 10) {
                    double avgEntropy = averageEntropy(data, i, j);

                    if (avgEntropy < minEntropy) {
                        minFeature = i;
                        minLimit = j;
                        minEntropy = avgEntropy;
                    }
                }
            }
        }

        return new DivisionPoint(minFeature, minLimit);
    }

    public static class DivisionPoint {
        private int featureId;
        private int limit;

        public DivisionPoint(int featureId, int limit) {
            this.featureId = featureId;
            this.limit = limit;
        }

        public boolean side(int[] point) {
            return point[featureId] >= limit;
        }
    }


    public Tree(List<Entry> data, boolean[] features) {
        root = new Node(new ArrayList<>(data));
        learn(features);
    }

    private void learn(boolean[] features) {
        Queue<Node> queue = new ArrayDeque<>();
        queue.offer(root);
        int i = 0;
        while (!queue.isEmpty()) {
            Node current = queue.poll();
            if (current.split(features)) {
                queue.offer(current.leftChild);
                queue.offer(current.rightChild);
                System.out.println(i);
                i++;
            }
        }
    }

    public int getClassId(int[] point) {
        return root.getClassId(point);
    }

}
