package org.tomat.spbsu.ml;

import org.tomat.spbsu.ml.hashing.util.Neighbours;
import org.tomat.spbsu.ml.knn.LSHKNNClassifier;
import org.tomat.spbsu.ml.knn.NaiveKNNClassifier;

import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
//        new Main().testHash();
//        new Main().runKNN();
        new Main().showClassifier();
    }

    public void showClassifier() throws IOException, ClassNotFoundException {
        InputStream str1 = Main.class.getClassLoader().getResourceAsStream("train-images.idx3-ubyte");
        InputStream str2 = Main.class.getClassLoader().getResourceAsStream("train-labels.idx1-ubyte");

        System.out.println("Loading...");
        ArrayList<Entry> entries = Loader.loadEntries(str1, str2, 10000);
        System.out.println("Done!");
        System.out.println("Testing...");
        LSHKNNClassifier classifier = loadLSHKNNClassifier(new File("7-70-1100.knn"));
        testKnn(classifier, entries, 20);
    }

    public LSHKNNClassifier loadLSHKNNClassifier(File fp) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(fp)));
        LSHKNNClassifier classifier = (LSHKNNClassifier) ois.readObject();
        return classifier;
    }

    public boolean saveClassifier(LSHKNNClassifier classifier) {
        try {
            FileOutputStream fos = new FileOutputStream(classifier.getK() + "-" + classifier.getL() + "-" + classifier.getRadius() + ".knn");
            BufferedOutputStream bo = new BufferedOutputStream(fos);
            ObjectOutputStream obj = new ObjectOutputStream(bo);
            obj.writeObject(classifier);
            obj.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public double testKnn(LSHKNNClassifier classifier, List<Entry> entries, int knn) {
        int testSize = entries.size();
        System.out.println("Testing started...");
        long start = System.nanoTime();
        int all = 0;
        int bad = 0;
        Collections.shuffle(entries);
        for (int i = 0; i < testSize; ++i) {
            all++;
            if (classifier.getClassId(entries.get(i).getPoint()) != entries.get(i).getClassId()) {
                bad++;
            }
        }
        long stop = System.nanoTime() - start;
        System.out.println("Sec per " + testSize + " = " + (double) stop / 1E9);
        System.out.println("K = " + classifier.getK() + " L = " + classifier.getL() + " radius = " + classifier.getRadius() + " kn = " + knn + " E = " + (double) bad / all);
        return (double) bad / all;
    }

    public void runKNN() throws IOException, ClassNotFoundException {
        int learnSize = 60000;
        int testSize = 10000;
//        int k = 7;
//        int l = 70;
//        int radius = 1100;
//        int knn = 30;

        InputStream str1 = Main.class.getClassLoader().getResourceAsStream("train-images.idx3-ubyte");
        InputStream str2 = Main.class.getClassLoader().getResourceAsStream("train-labels.idx1-ubyte");

        System.out.println("Loading learn data...");
        ArrayList<Entry> entries = Loader.loadEntries(str1, str2, learnSize);
        System.out.println("Learn data loaded!");

        double prev = Double.POSITIVE_INFINITY;
        double cur = Double.POSITIVE_INFINITY;


        for (int k = 8; k >= 5; --k) {
            for (int l = 50; l < 150; l += 40) {
                for (int r = 1000; r < 1500; r += 100) {
                    System.out.println("Started learning...");
                    LSHKNNClassifier classifier = new LSHKNNClassifier(entries, k, l, r, 20);
                    System.out.println("Learning finished!");

                    System.out.println("Writing objects...");
                    saveClassifier(classifier);
                    System.out.println("Done!");
                    for (int knn = 10; knn < 50; knn += 10) {
                        classifier.setNei(knn);
                        cur = testKnn(classifier, entries.subList(0, testSize), knn);
                    }
                }
            }
        }

    }


    public void testHash() throws IOException {
        int learnSize = 60000;
        int checkData = 10000;
        int k = 7;
        int l = 70;
        int radius = 1100;

        InputStream str1 = Main.class.getClassLoader().getResourceAsStream("train-images.idx3-ubyte");
        InputStream str2 = Main.class.getClassLoader().getResourceAsStream("train-labels.idx1-ubyte");

        List<Entry> entries = Loader.loadEntries(str1, str2, 60000);
        Collections.shuffle(entries);
        entries = entries.subList(0, learnSize);

        Neighbours n = new Neighbours(entries, k, l, radius);

        double SEFails = 0;
        double SECheckSize = 0;
        double SENeighbours = 0;

        Collections.shuffle(entries);

        for (int i = 0; i < checkData; ++i) {
            int[] point = entries.get(i).getPoint();
            Collection<Entry> nei = n.getNN(point);

            SECheckSize += (double) nei.size() / learnSize;

            List<Entry> real = findByRadius(entries, point, radius);
            int should = real.size();

            //nei.retainAll(real);
            SENeighbours += (double) nei.size();

            real.removeAll(nei);
            SEFails += (double) real.size() / should;
        }
        SENeighbours /= checkData;
        SECheckSize /= checkData;
        SEFails /= checkData;

        System.out.println("k = " + k);
        System.out.println("l = " + l);
        System.out.println("SENeighbours = " + SENeighbours);
        System.out.println("SECheckSize = " + SECheckSize);
        System.out.println("SEFails = " + SEFails);
        System.out.println();
    }

    public List<Entry> findByRadius(List<Entry> entries, int[] point, long r) {
        ArrayList<Entry> result = new ArrayList<>();
//        for (Entry e : entries) {
//            if (NaiveKNNClassifier.squareDistanceTo(point, e.getPoint()) <= r * r) {
//                result.add(e);
//            }
//        }
        return result;
    }
}
