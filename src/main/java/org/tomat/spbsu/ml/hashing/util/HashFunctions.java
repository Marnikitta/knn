package org.tomat.spbsu.ml.hashing.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.function.Function;

public final class HashFunctions {
    public static Function<int[], Integer> generateRandProjHashL2(int step, int dim) {
        Random rd = new Random();
        double[] dir = new double[dim];
        for (int i = 0; i < dim; ++i) {
            dir[i] = rd.nextGaussian();
        }
        return new RandProj(dir, step);
    }

    public static class RandProj implements Function<int[], Integer>, Serializable {
        private final double[] dir;
        private final int step;
        private final int bias;

        public RandProj(double[] dir, int step) {
            Random rd = new Random();
            this.dir = dir;
            this.step = step;
            this.bias = rd.nextInt(step);
        }

        @Override
        public Integer apply(int[] ints) {
            int projLength = 0;

            if (dir.length != ints.length) {
                throw new IllegalArgumentException();
            }
            for (int i = 0; i < dir.length; ++i) {
                projLength += dir[i] * ints[i];
            }

            return Math.floorDiv(projLength + bias, step);
        }
    }

    public static Function<int[], ArrayList<Integer>> generateRandProjVec(int step, int dim, int k) {
        Function<int[], Integer>[] hF = new Function[k];
        for (int i = 0; i < k; ++i) {
            hF[i] = generateRandProjHashL2(step, dim);
        }
        return new RandProjVector(hF);
    }

    public static class RandProjVector implements Function<int[], ArrayList<Integer>>, Serializable {
        private int k;
        public Function<int[], Integer>[] functions;

        public RandProjVector(Function<int[], Integer>[] functions) {
            this.functions = functions;
            this.k = functions.length;
        }

        @Override
        public ArrayList<Integer> apply(int[] ints) {
            ArrayList<Integer> result = new ArrayList<>(k);
            for (int i = 0; i < k; ++i) {
                result.add(functions[i].apply(ints));
            }
            return result;
        }
    }
}
