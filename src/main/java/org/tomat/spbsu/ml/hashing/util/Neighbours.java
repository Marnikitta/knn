package org.tomat.spbsu.ml.hashing.util;

import org.tomat.spbsu.ml.Entry;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;

public class Neighbours implements Serializable {
    private int l;
    private HashMap<List<Integer>, ArrayList<Entry>>[] hashSets;
    private Function<int[], ArrayList<Integer>>[] functions;

    public Neighbours(List<Entry> entries, int k, int l, int r) {
        this.l = l;
        int dim = entries.get(0).getDim();

        functions = new Function[l];
        hashSets = new HashMap[l];
        for (int i = 0; i < l; ++i) {
            functions[i] = HashFunctions.generateRandProjVec(randStep(r), dim, k);
        }
        for (int i = 0; i < l; ++i) {
            hashSets[i] = new HashMap<>();
        }
        int i = 0;
        for (Entry e : entries) {
            ++i;
            putEntryToBuckets(e);
            if (i % 1000 == 0) {
//                System.out.println(i);
            }
        }
    }

    public int randStep(int r) {
        Random rd = new Random();
        int eps = r / 2;
        return rd.nextInt(eps * 2) + 2 * r - eps;
    }

    public void putEntryToBuckets(Entry e) {
        for (int i = 0; i < l; ++i) {
            ArrayList<Integer> hash = functions[i].apply(e.getPoint());
            ArrayList<Entry> b = hashSets[i].get(hash);
            if (b != null) {
                b.add(e);
            } else {
                b = new ArrayList<>();
                b.add(e);
                hashSets[i].put(hash, b);
            }
        }

    }

    public HashSet<Entry> getNN(int[] point) {
        HashSet<Entry> result = new HashSet<>();
        for (int i = 0; i < l; ++i) {
            ArrayList<Integer> hf = functions[i].apply(point);
            ArrayList<Entry> curr = hashSets[i].get(hf);
            if (curr != null) {
                result.addAll(curr);
            }
        }
        return result;
    }
}
